"use strict";
// Constants used as KEYS for LocalStorage
const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIPS_DATA_KEY = "tripsLocalData";
const MAP_BOX_TOKEN =
  "pk.eyJ1IjoidmV0aG1hIiwiYSI6ImNrbzdidDRwcDF0YWEycm1iNXQ1ZzZlOWcifQ.BpJfQGTt2QKP7vCJ8UtP9A";
var TRIPS = [];
// The following code is to implement the Trip class
class Trip 
{
  constructor(
    id,
    country,
    dateTime,
    origin,
    destination,
    noOfStops,
    tripSpots
  ) {
    this._id = id;
    this._country = country;
    this._date = dateTime;
    this._origin = origin;
    this._destination = destination;

    this._noOfStops = noOfStops;

    this._tripSpots = tripSpots;
  }
}
//The following code is to implement the TripList class
class TripList 
{
  constructor() 
  {
    this._trips = [];
  }

  get getTrips()
  {
    return this._trips;
  }

  addTrip(trip) 
  {
    this._trips.push(trip);
  }

  removeTrip(id) 
  {
    this._trips = this._trips.filter((trip) => Number(trip._id) !== Number(id));
  }
}